# ivy-magento-console.el

`ivy-magento-console.el` can be used to interact with the Magento console using Emacs. Simply call `ivy-magento-console`, type the command you wish to execute and then it'll run in a shell, in the other window. To get help for a specific command, press `M-o` and then `h`. Some commands require additional input and will ask for it. However, you're able to use a universal argument (`C-u`) to provide input to any command.

## Installation

To install:
1) Clone this repository
2) Run `M-x RET package-install-file` in Emacs and direct it to ivy-magento-console.el
3) Add `(require 'ivy-magento-console)` to your init file

## Images

Showing the commands in the minibuffer:

![Showing the commands in the minibuffer](images/ivy-magento-minibuffer.png)

Executing a command:

![Executing a command](images/ivy-magento-executing-command.png)

Displaying help information:

![Displaying help information](images/ivy-magento-help.png)
